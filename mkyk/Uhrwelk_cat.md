ゆかちんと喧嘩した。

はじめは些細なことだった。
ゆかちんの細かいところが気になって、注意しただけだったのに。


『ゆかちん、前から気になってたんだけどさ…』


気が付いたらお互いに不満をぶちまけていて。


『どうしていつもみかしーは…！』

『ゆかちんだって…！』


わたしは思わず、枕元にあった時計を投げつけた。


『もうゆかちんなんか嫌いだ！！』

『痛っ…！』


投げた時計はゆかちんに当たって、床に落ちて、なにかが壊れた音がした。


『もういいよみかしーなんか！』

『こっちこそ！』


売り文句に買い文句。
とうとうゆかちんは泣き出して、家を飛び出していった。


あれから何時間経ったのだろうか。床の上にある、わたしたち二人の時を刻んでいた時計。唯一時を知ることのできたその時計は、あの時のまま止まっている。

わたしたち、これで終わりなのかな？
止まった時計を眺めて、そんなことを考える。全てわたしが悪いんだ。関係を壊してしまったのはわたしだ。この時計のように。

ふと周りを見渡すと、二人で撮った写真が目に入った。猫カフェで、猫に頬ずりするゆかちんとわたし。


『みかしー、ほらほらこの猫！真っ白でもふもふー！』

『こっちの黒猫もかっこかわいいなぁ…！』


猫はとってもかわいくて、人懐っこくて、それでいておとなしくて…。

ゆかちんも猫になればいいのに。
そんな考えが、ふと頭をよぎった。
声にして、つぶやいてみる。


「ゆかちん、猫になっちゃえ」


ドアの外から、鳴き声が聴こえた。

「この子、どうしよう…」


まさか、この世界にはほんとに魔法が存在するのだろうか。
わたしの膝の上にいる白い猫は、そんな疑問を投げかける。

猫になれ。
そうつぶやいた直後、ドアの外から鳴き声がすると思ったら、白い猫がうずくまっていたのだ。
思わず抱き上げて家に入れてしまったけど、もしほかの家の飼い猫だったらどうしよう…？でも首輪はないし…。
そんな心配をよそに、わたしの右手を舐める猫。
いつものゆかちんのような、無邪気な顔をして、わたしを見つめる。それがゆかちんと重なって、目をそらした。


でも、もしこの猫がほんとにゆかちんだとしたら、このままでも、いいかも。
いつもわたしの言う通りにしてくれて、家に帰ってきたわたしを出迎えてくれて、わたしを癒してくれて…。
猫になったゆかちんは、そんな理想的な存在なんだよ？
自分の問いかけが通じたのか、猫は小さく鳴く。


「じゃあ、今から君に『ゆか』って名付けてあげよう」


『ゆか』は、わたしの言葉が通じるのか、再び鳴いた。どうやらこの子はほんとにゆかちんで、ほんとにわたしは魔法使いらしい。
それなら、明日はホームセンターに行って、ゆかの餌とか、小屋とか、いろいろ必要なものを揃えなきゃ。楽しみだね、ゆか。
あ、わたしが自分に魔法をかけて、猫2匹で仲良く暮らすってのも、いいかもしれないなぁ。そしたら、猫同士だし、ゆかと分かり合えるかもしれない。
でもそれじゃあ、餌もないし死んじゃうかな？じゃあ明日たくさん餌を買い込んでおこうか。それがなくなるまで、ゆかとわたし、仲良く暮らせるね。
お仕事はどうなっちゃうかな？わたしもゆかもできなくなるね。急に消えたってなって、騒がれるかも。みんなに迷惑かけちゃうなぁ…。
妄想はどこまでも、果てしなく進んでいく。


でも。
やっぱりわたしは人間でいたい。
人間のまま、ゆかちんに、ごめんって言いたい。
わたしは、『ゆか』じゃなくて、『由佳』と仲直りしたいんだ。


『ゆか』を『由佳』に戻すには、どうしたらいいのだろう。ゆかは屈託のない声で鳴いて、わたしの手を舐めるばかりで、何も答えてくれない。


「わたしに魔法の解き方を教えてよ、ゆか」


ニャーじゃわかんないよ、ゆか…。
涙がとめどなく溢れてくる。ゆかちんにしたことに対する罪悪感、後悔、これからのことを考えたら、何の解決にもならないのに、ただ泣くしかできなかった。


「ごめんね、ごめんね、由佳…」

「…こっちこそごめん」

「ふぇ？」


どれくらい泣いていたのだろう。
気がつくと、猫になったはずのゆかちんが、目の前に立っていた。


「え、なんで…だってゆかちんは猫になって」

「何言ってるの、みかしー？」

「…あぁごめん、何でもない。あの、その…いろいろとごめん」

「あー、いやこっちもその…悪いところあったしさ。これから気を付けるから、みかしー仲直りしよ？」

「…うん。ありがと、ゆかちん。ねぇ、この猫飼おうか！」

「え、この猫どうしたの？」

「この猫はゆかちんなんだよー」

「ん？どゆこと？」


涙を拭きながら、床に転がる時計を拾う。なんだ、電池が外れてただけか。

電池を戻して、時間を合わせて。

二人に、一匹の猫を加えて。

時はまた、刻み始める。