「ねぇるみちゃん」

「…なに」

「いいの、あのままで？」

「あんたには関係ないでしょ」


膝の上にちょこんと座った『それ』と、会話を交わす。
それは白くて、もふもふしてて…。


「それよりさ」

「ん？」

「なんで喋れるの？」


私は、うさぎと話しています。


≪満月のよるに≫


津田ちゃんと喧嘩した。
初めはどうでもいいことだったのに、次第に言い合いになっていて、気付けば私は大雨の中、津田ちゃんの家を飛び出していた。
ずぶ濡れになりながら、何日振りかも分からない家路の途中。
微かに、でも確かに、何かの鳴き声を聞いた。
声を頼りに路地を行くと、段ボール。
中からはうさ、うさと聞こえる。
……うさ？
開けてみると、ずぶ濡れになってるうさぎが一匹。
まさか、うさぎを捨てる飼い主がいるなんて…。
このままでは死んでしまうだろう、私は兎を家に連れていくことにした。
家に着き、シャワーを浴びて、人参を兎に食べさせながら、私はこれからどうしたものかと悩む。
ねぇうさぎさん、聞いてくれる？


「うさぎさんの名前はなにかなー」

「つきうさぎです」

「…は？」

「いや、だから月兎です。月の兎で」

「そんなことはどうでもいいわよ！なんであんた…」

「月の兎は賢いのです」

「お…おう」

「それよりるみちゃん」

「なんで私の名前知ってんの！？あと気安く呼ぶな！」

「あのままでいいの？」


そういうとうさぎさん───月兎は、私の携帯を差す。
さっきから鳴り止まないそれは、津田ちゃんからの着信でいっぱいになっているだろう。


「……あぁ、いいのよあれは」

「よくないでしょ」

「あんたに何がわかるって言うのよ！」

「分かるよ。僕は賢いから。どうして人間は好きだというのにこう遠回りするんだろうね？」

「……」

「だからさ、僕が手助けしてあげるよ。二人の仲直り」

「…ほんと？」


というわけで、この喋るうさぎさんの提案に乗ることになったのだが…。


｢ほんとにこんなので仲直りできるんでしょうね！？｣

｢え？もちろんだよ。僕は賢いからね｣

｢賢い賢いうるさい！｣

｢口より先に手を動かして｣

｢ぐぬぬ…｣


うさぎにペースを握られる人間はきっとこの世で私しかいないだろう。
そんなこんなで数時間後。


｢ふぅ、できた…｣

｢お疲れ様。さぁ、行こうか｣

｢え、だってもう夜だよ｣

｢そんなのは関係ないの。今日じゃなきゃダメなんだから｣

｢…それもそうね｣


作った『それ』を持つと、私は再び、津田ちゃんの家へと歩き出した。


｢ってなんであんたもついてくるのよ！？｣

｢え？いいじゃん別に｣

｢……｣


兎を肩に乗せて、手には作ったばかりの『それ』。うぅ、恥ずかしい…。
うつむき気味で歩いていると、もう津田ちゃんの家の前。


｢ほら、早く｣

｢分かったわよ…ちょっと邪魔だから降りてて｣


インターホンを押す。幸いにも津田ちゃんはまだ寝ていなかったようだ。


『はい』

｢…大久保です｣

『…どうしたの』

｢…話が、あってね｣

｢……とりあえず入りなよ｣


ドアを開けた津田ちゃんは、私の持つ月見団子と、肩に載る白いうさぎに驚いたことだろう。


｢えーと…どういうこと｣

｢いや、このうさぎがさ…作れって言ってさ…｣

｢…？｣

｢……｣


…いや、毛づくろいしてないでなにか喋ってよ！！


｢このうさぎ、喋るの？｣

｢いや、まぁ冗談だよ、ははは…｣


どうやらこの月兎とやらは、私たちが言葉を交わすためのネタとしてはいい働きをしたようで。
この流れに乗って、そのまま…。


『いいかい？ちゃんと言うんだよ』


そう、耳元でささやかれた気がした。


｢あのさ！さっきは、ごめん｣

｢…あぁ｣

｢そのさ、私も津田ちゃんのこと、よく考えないで…｣

｢いいよ、というかこっちこそごめん｣

｢……｣

｢ほら、寒いから入りなよ｣

｢…うん。それでさ｣

｢？｣

｢お月見でも、しようかなって｣


そう言って、お団子を差し出す。


｢ん、分かった｣


ベランダのカーテンを開けると、さっきまで雨が降っていたとは思えないほど、きれいな満月が見えた。
そこに作ってきたお団子を置いて、その前に二人(と一匹)、ちょこんと座る。
月の光に照らされた津田ちゃんはこの世の何よりも綺麗で、思わず見とれてしまう。

そうだ、このうさぎさんが、私の背中を押してくれたんだよ。
ほんとは喋るんだ、このうさぎ。
それでね、今日は一緒にお団子を作ってね、それでね。
でさ、このうさぎ、一緒に飼う気ない？
もし津田ちゃんがいいなら…。

………
……
…


気がつくと、私は津田ちゃんに寄り添いながら、眠りこけてしまったらしい。


｢…あっ、ごめん｣

｢あ、起きた？｣

｢あれ、あのうさぎ…｣


まわりを見ると、あの月兎さんがいない。


｢…なんのこと？｣

｢…え？｣


…やっぱり、言葉を喋るうさぎなんて、幻だったのだろうか。
でも、私たちの前にある月見団子は、それが幻でないことを物語っていて…。


｢ねぇ、るみちゃん｣

｢ん…？｣

｢月が…綺麗、ですね｣

｢……そうだね｣


ありがとう、月兎さん。
私たち、さっきよりずっと仲良しになれたみたいです。

月に映るうさぎが、ぴょんと跳ねた気がした。